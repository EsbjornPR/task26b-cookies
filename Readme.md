# Task 26B Cookies
## The task
Create a cookie manager as a class named "CookieManager" with three methods:
1) setCookie(name, data)
2) getCookie(name)
3) clearCookie(name)

###### The implementation
The class with it's method is found in the top of index.js.
I've choosen to make static methods so that you can reach them directly via the CookieManager without creating an instance.
