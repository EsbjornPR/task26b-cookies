class CookieManager {
    // Method
    static setCookie(name, data) {
        document.cookie = `${name}=${data}; max-age=900`; 
    }
    static getCookie(name) {
        let cookieArr = document.cookie.split('; ');
        let nameValuePair = [];
        //let returnValue = false;
        for (let i = 0; i < cookieArr.length; i++) {
            nameValuePair = cookieArr[i].split('=');
            if ( name === nameValuePair[0] ) {
                return nameValuePair[1];
            }
        }
        return false;
    }
    static clearCookie(name) {
        document.cookie = `${name}=; max-age=-900`; 
    }
}



function init() {
    sessionStorage.setItem('name', 'Elon Musk');
    sessionStorage.setItem('number', 007);
    // console.log(sessionStorage.getItem('name'));
    // console.log(sessionStorage.getItem('number'));

    CookieManager.setCookie( "Bil", "Tesla");
    CookieManager.setCookie('Yrke', 'Serie figur');
    console.log(CookieManager.getCookie("Bil"));
    CookieManager.clearCookie("Yrke");

}



